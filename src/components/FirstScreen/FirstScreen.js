import React from 'react';
import Styled from 'styled-components';
import config from 'visual-config-exposer';

import SoundOn from '../../../public/assets/volume-2.svg';
import SoundOff from '../../../public/assets/volume-x.svg';

import './FirstScreen.css';

const First = Styled.div`
background: url(${config.settings.firstImage});
background-size: cover;
background-repeat: no-repeat;
background-position: center;
`;

const Button = Styled.button`
background-color: ${config.settings.btnBackground};
color: ${config.settings.btnFontColor};
`;

const Heading = Styled.h1`
color: ${config.settings.captionColor};
`;

const FirstScreen = (props) => {
  let image = props.soundOn ? SoundOn : SoundOff;

  const soundChangeHandler = () => {
    props.soundHandler();
  };

  return (
    <First className="first-screen">
      <Heading className="first-heading">
        Tap {config.settings.clicks - props.clicks + 1} times to{' '}
        {config.settings.captionTitle}
      </Heading>
      <Button className="first-btn" onClick={props.clickCounter}>
        {config.settings.btnText}
      </Button>
      <div className="sound__image" onClick={soundChangeHandler}>
        <img src={image} alt="sound" />
      </div>
    </First>
  );
};

export default FirstScreen;
